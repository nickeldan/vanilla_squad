CFLAGS := -std=gnu99 -fdiagnostics-color -Wall -Wextra -Werror
ifeq ($(debug),yes)
    CFLAGS += -O0 -g -DDEBUG
else
    CFLAGS += -O2 -DNDEBUG
endif

all: _all

BUILD_DEPS :=
ifeq ($(MAKECMDGOALS),clean)
else ifeq ($(MAKECMDGOALS),format)
else
    BUILD_DEPS := yes
endif

VASQ_DIR := .
include make.mk

TEST_DIR := tests
include tests/make.mk

.PHONY: all _all format clean

_all: $(VASQ_SHARED_LIBRARY) $(VASQ_STATIC_LIBRARY)

format:
	@find . -name '*.[hc]' -print0 | xargs -0 -n 1 clang-format -i

clean: $(CLEAN_TARGETS)
	@rm -f $(DEPS_FILES)
